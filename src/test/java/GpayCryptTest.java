
import org.junit.jupiter.api.Test;

import java.security.GeneralSecurityException;
import java.util.ArrayList;
import java.util.List;


class GpayCryptTest {
    @Test
    void testDecrypt() throws Exception {
        GpayCrypt crypt = new GpayCrypt("/Users/mutemi/Dev/Dibsy/Docs/pkcs8_key.pem", "dibsy", GpayCrypt.KeysManager.INSTANCE_PROD);
        Decryptor decryptor = crypt.getDecryptor();

        List<String> payloads = new ArrayList<>();
        payloads.add("""
                {"signature":"MEQCIAgFQDdRHsumZsAZ7bkrtzrApWd+XqysfmENYL32RlFAAiBrJsj3cg7b1UHztjn9ObPXEhj8EgnaXYZZkjhJtisiHA\\u003d\\u003d","intermediateSigningKey":{"signedKey":"{\\"keyValue\\":\\"MFkwEwYHKoZIzj0CAQYIKoZIzj0DAQcDQgAEugbWttpkZYhIUDryaoU8LQYhPc31zJv5qnav6IVvwCrBC/rB6bY9TrbTC49U9cvbjBTWB7cAU7Ku55HAvN22/Q\\\\u003d\\\\u003d\\",\\"keyExpiration\\":\\"1690331084500\\"}","signatures":["MEQCICWLRwV6sI7QhRSYP+7aV/5twRK0Tl1nSJL82biIY4m0AiARCfbbQZeE2dUrQZbZUS8rgd3F6/EeqvMaG4cNn+/aew\\u003d\\u003d"]},"protocolVersion":"ECv2","signedMessage":"{\\"encryptedMessage\\":\\"sj6gHOW0DGHkwf4A/JOzMFXI+5sWyOqWphPdI+F6sbb+x13WxzxAt6uSVGZRQoYjPpEPEdsaQoaV3jKckTn6M+OxLKty4LT4OyLSe0U4nwgYQs2LQiqikSVLpARFBLa/8SqgpuTD8wm0IsXPqfYS3Ogwfk9z/IaaeYlLPLmTe3LdQOipiCUtkj+UaKa4leFy3gLjacoqlaUmK07yvlRNtKkkupaqSK8JjHe+SMrlta3zsp+xsSiuqCPOFG6k+2kKIseoRZhV68JlFW+WJI2y/oGyUUiQp9GT2x3PRWKQj2ijL04LsaUsttgPXW6NMNjr1tJzqgg3V0qd02DFSAcZadLZRSLUMG48ZCPcGCXqUJ6517tkErvBFf7v72U4GChRYDb6QwPvSyis2+q4HCtcQBVZNSRdWiZdLI1lyieCMeA1abU4Nme7IlIJdJ85rafKj33OamR7K0dE5lNCKFKbmoQNS6X8+fvWxX76qTIG8pROyUeE+6DVM2Zlscix+OY+Kl1ycB72Mn/3doQ7Rda+t7ZsaRqIeE5Dt81s8XwLMGQV7nVnlUTaxGiBAwYH1JlxPfKWcmmPKo6E/6yUJA0ATA6ls57igONrK1QlUjOuCCZ5HGJcK74T3ToptESZeHL1pi3/Shqjf6ib/A\\\\u003d\\\\u003d\\",\\"ephemeralPublicKey\\":\\"BP/xLrHMWOaIXmg4ltyNU2Cbp/h0OXqCG0PzCz4eZxe5PkGD67kH2PUrRcDBoZEMJYFXu4Cl68v0wFARAshvErs\\\\u003d\\",\\"tag\\":\\"icZTrSBSkPzLEYW7QEmmvEGEkRo784rTdRiScwS5L80\\\\u003d\\"}"}
                """);
        // test decrypt
        for (String payload : payloads) {
            try {
                decryptor.run(payload);
            } catch (GeneralSecurityException e) {
                e.printStackTrace();
            }
        }

    }
}