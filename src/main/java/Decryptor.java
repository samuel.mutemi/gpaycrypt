import com.google.crypto.tink.apps.paymentmethodtoken.GooglePaymentsPublicKeysManager;
import com.google.crypto.tink.apps.paymentmethodtoken.PaymentMethodTokenRecipient;

import java.security.GeneralSecurityException;

public class Decryptor {
    private final String privateKey;
    private final GooglePaymentsPublicKeysManager keysManager;
    private final String merchantId;

    public Decryptor(String privateKey, GooglePaymentsPublicKeysManager keysManager, String merchantId) {
        this.privateKey = privateKey;
        this.keysManager = keysManager;
        this.merchantId = merchantId;
    }

    String run(String encryptedMessage) throws GeneralSecurityException {
        System.out.println("Attempting to decrypt message: " + encryptedMessage);
        String decryptedMessage =
                new PaymentMethodTokenRecipient.Builder()
                        .fetchSenderVerifyingKeysWith(
                                keysManager)
                        .recipientId("gateway:" + merchantId)
                        // This guide applies only to protocolVersion = ECv2
                        .protocolVersion("ECv2")
                        // Multiple private keys can be added to support graceful
                        // key rotations.
                        .addRecipientPrivateKey(privateKey)
                        .build()
                        .unseal(encryptedMessage);
        System.out.println("Decrypted message: " + decryptedMessage);
        return decryptedMessage;
    }
}
