import KeyReader.PrivateKeyReader;
import com.google.crypto.tink.apps.paymentmethodtoken.GooglePaymentsPublicKeysManager;

public class GpayCrypt {

    public enum KeysManager {
        INSTANCE_TEST,
        INSTANCE_PROD
    }


    final String privateKey;
    final GooglePaymentsPublicKeysManager keysManager;
    final String merchantId;

    public GpayCrypt(String privateKeyFilePath, String merchantId) throws Exception {
        this.privateKey = PrivateKeyReader.get(privateKeyFilePath);
        this.keysManager = GooglePaymentsPublicKeysManager.INSTANCE_TEST;
        this.merchantId = merchantId;
        init(KeysManager.INSTANCE_TEST);
    }

    public GpayCrypt(String privateKeyFilePath, String merchantId, KeysManager keysManager) throws Exception {
        this.privateKey = PrivateKeyReader.get(privateKeyFilePath);
        this.keysManager = getKeysManager(keysManager);
        this.merchantId = merchantId;
        init(keysManager);
    }

    void init(KeysManager keysManager) {
        if(keysManager == KeysManager.INSTANCE_PROD)
            GooglePaymentsPublicKeysManager.INSTANCE_PRODUCTION.refreshInBackground();
        else
            GooglePaymentsPublicKeysManager.INSTANCE_TEST.refreshInBackground();
    }

    Decryptor getDecryptor() {
        return new Decryptor(privateKey, keysManager, merchantId);
    }

    private GooglePaymentsPublicKeysManager getKeysManager(KeysManager keysManager) {
        if (keysManager == KeysManager.INSTANCE_TEST) {
            return GooglePaymentsPublicKeysManager.INSTANCE_TEST;
        } else if (keysManager == KeysManager.INSTANCE_PROD) {
            return GooglePaymentsPublicKeysManager.INSTANCE_PRODUCTION;
        } else {
            return GooglePaymentsPublicKeysManager.INSTANCE_TEST;
        }
    }
}
