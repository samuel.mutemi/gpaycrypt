package KeyReader;

import java.nio.file.Files;
import java.nio.file.Paths;

public class PrivateKeyReader {

        public static String get(String filename)
                throws Exception {

            byte[] keyBytes = Files.readAllBytes(Paths.get(filename));

            var cleanKeyString = new String(keyBytes)
                    .replace("-----BEGIN PRIVATE KEY-----", "")
                    .replace("-----END PRIVATE KEY-----", "");

                System.out.println("private key : " + cleanKeyString);
            return cleanKeyString;
        }

}
