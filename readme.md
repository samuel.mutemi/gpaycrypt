### GpayCrypt
GpayCrypt is a simple and easy to use encryption and decryption tool for Google Pay.

## Usage
Use as a library. 
Example in the tests. 
To install dependencies use the following command:

```bash
mvn clean install
```

To run tests use the following command:

```bash
mvn test
```

## Note
Works with PKCS8 keys.
To transform EC keys to PKCS8 use the following command:
```bash
openssl pkcs8 -topk8 -nocrypt -in ec_key.pem -out pkcs8_key.pem
```
